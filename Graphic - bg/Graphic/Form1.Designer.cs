﻿namespace Graphic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea31 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend31 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series31 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea32 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend32 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series32 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea33 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend33 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series33 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea34 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend34 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series34 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea35 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend35 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series35 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea36 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend36 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series36 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea37 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend37 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series37 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea38 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend38 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series38 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea39 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend39 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series39 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea40 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend40 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series40 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea41 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend41 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series41 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea42 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend42 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series42 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea43 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend43 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series43 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea44 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend44 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series44 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea45 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend45 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series45 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart7 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart8 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart9 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart10 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chart11 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart12 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart13 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart14 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart15 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart15)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea31.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea31);
            legend31.Name = "Legend1";
            this.chart1.Legends.Add(legend31);
            this.chart1.Location = new System.Drawing.Point(178, 12);
            this.chart1.Name = "chart1";
            series31.ChartArea = "ChartArea1";
            series31.Legend = "Legend1";
            series31.Name = "Series1";
            this.chart1.Series.Add(series31);
            this.chart1.Size = new System.Drawing.Size(1057, 129);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea32.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea32);
            legend32.Name = "Legend1";
            this.chart2.Legends.Add(legend32);
            this.chart2.Location = new System.Drawing.Point(178, 147);
            this.chart2.Name = "chart2";
            series32.ChartArea = "ChartArea1";
            series32.Legend = "Legend1";
            series32.Name = "Series1";
            this.chart2.Series.Add(series32);
            this.chart2.Size = new System.Drawing.Size(1057, 129);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // chart3
            // 
            chartArea33.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea33);
            legend33.Name = "Legend1";
            this.chart3.Legends.Add(legend33);
            this.chart3.Location = new System.Drawing.Point(178, 282);
            this.chart3.Name = "chart3";
            series33.ChartArea = "ChartArea1";
            series33.Legend = "Legend1";
            series33.Name = "Series1";
            this.chart3.Series.Add(series33);
            this.chart3.Size = new System.Drawing.Size(1057, 129);
            this.chart3.TabIndex = 2;
            this.chart3.Text = "chart3";
            // 
            // chart4
            // 
            chartArea34.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea34);
            legend34.Name = "Legend1";
            this.chart4.Legends.Add(legend34);
            this.chart4.Location = new System.Drawing.Point(178, 417);
            this.chart4.Name = "chart4";
            series34.ChartArea = "ChartArea1";
            series34.Legend = "Legend1";
            series34.Name = "Series1";
            this.chart4.Series.Add(series34);
            this.chart4.Size = new System.Drawing.Size(1057, 129);
            this.chart4.TabIndex = 3;
            this.chart4.Text = "chart4";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(14, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 32);
            this.button1.TabIndex = 4;
            this.button1.Text = "Избор на файл";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1051, 551);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(184, 40);
            this.button3.TabIndex = 6;
            this.button3.Text = "Следващи графики";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(849, 552);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(184, 40);
            this.button4.TabIndex = 7;
            this.button4.Text = "Предишни графики";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // chart5
            // 
            chartArea35.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea35);
            legend35.Name = "Legend1";
            this.chart5.Legends.Add(legend35);
            this.chart5.Location = new System.Drawing.Point(178, 417);
            this.chart5.Name = "chart5";
            series35.ChartArea = "ChartArea1";
            series35.Legend = "Legend1";
            series35.Name = "Series1";
            this.chart5.Series.Add(series35);
            this.chart5.Size = new System.Drawing.Size(510, 129);
            this.chart5.TabIndex = 8;
            this.chart5.Text = "chart5";
            // 
            // chart6
            // 
            chartArea36.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea36);
            legend36.Name = "Legend1";
            this.chart6.Legends.Add(legend36);
            this.chart6.Location = new System.Drawing.Point(725, 417);
            this.chart6.Name = "chart6";
            series36.ChartArea = "ChartArea1";
            series36.Legend = "Legend1";
            series36.Name = "Series1";
            this.chart6.Series.Add(series36);
            this.chart6.Size = new System.Drawing.Size(510, 129);
            this.chart6.TabIndex = 9;
            this.chart6.Text = "chart6";
            // 
            // chart7
            // 
            chartArea37.Name = "ChartArea1";
            this.chart7.ChartAreas.Add(chartArea37);
            legend37.Name = "Legend1";
            this.chart7.Legends.Add(legend37);
            this.chart7.Location = new System.Drawing.Point(178, 12);
            this.chart7.Name = "chart7";
            series37.ChartArea = "ChartArea1";
            series37.Legend = "Legend1";
            series37.Name = "Series1";
            this.chart7.Series.Add(series37);
            this.chart7.Size = new System.Drawing.Size(1057, 187);
            this.chart7.TabIndex = 10;
            this.chart7.Text = "chart7";
            // 
            // chart8
            // 
            chartArea38.Name = "ChartArea1";
            this.chart8.ChartAreas.Add(chartArea38);
            legend38.Name = "Legend1";
            this.chart8.Legends.Add(legend38);
            this.chart8.Location = new System.Drawing.Point(178, 211);
            this.chart8.Name = "chart8";
            series38.ChartArea = "ChartArea1";
            series38.Legend = "Legend1";
            series38.Name = "Series1";
            this.chart8.Series.Add(series38);
            this.chart8.Size = new System.Drawing.Size(1057, 187);
            this.chart8.TabIndex = 11;
            this.chart8.Text = "chart8";
            // 
            // chart9
            // 
            chartArea39.Name = "ChartArea1";
            this.chart9.ChartAreas.Add(chartArea39);
            legend39.Name = "Legend1";
            this.chart9.Legends.Add(legend39);
            this.chart9.Location = new System.Drawing.Point(178, 12);
            this.chart9.Name = "chart9";
            series39.ChartArea = "ChartArea1";
            series39.Legend = "Legend1";
            series39.Name = "Series1";
            this.chart9.Series.Add(series39);
            this.chart9.Size = new System.Drawing.Size(1057, 247);
            this.chart9.TabIndex = 12;
            this.chart9.Text = "chart9";
            // 
            // chart10
            // 
            chartArea40.Name = "ChartArea1";
            this.chart10.ChartAreas.Add(chartArea40);
            legend40.Name = "Legend1";
            this.chart10.Legends.Add(legend40);
            this.chart10.Location = new System.Drawing.Point(178, 299);
            this.chart10.Name = "chart10";
            series40.ChartArea = "ChartArea1";
            series40.Legend = "Legend1";
            series40.Name = "Series1";
            this.chart10.Series.Add(series40);
            this.chart10.Size = new System.Drawing.Size(1057, 247);
            this.chart10.TabIndex = 13;
            this.chart10.Text = "chart10";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(14, 114);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(148, 32);
            this.button5.TabIndex = 14;
            this.button5.Text = "Изход";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(14, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 32);
            this.button2.TabIndex = 15;
            this.button2.Text = "Запис на екран";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chart11
            // 
            chartArea41.Name = "ChartArea1";
            this.chart11.ChartAreas.Add(chartArea41);
            legend41.Name = "Legend1";
            this.chart11.Legends.Add(legend41);
            this.chart11.Location = new System.Drawing.Point(178, 12);
            this.chart11.Name = "chart11";
            series41.ChartArea = "ChartArea1";
            series41.Legend = "Legend1";
            series41.Name = "Series1";
            this.chart11.Series.Add(series41);
            this.chart11.Size = new System.Drawing.Size(1057, 534);
            this.chart11.TabIndex = 17;
            this.chart11.Text = "chart11";
            // 
            // chart12
            // 
            chartArea42.Name = "ChartArea1";
            this.chart12.ChartAreas.Add(chartArea42);
            legend42.Name = "Legend1";
            this.chart12.Legends.Add(legend42);
            this.chart12.Location = new System.Drawing.Point(178, 13);
            this.chart12.Name = "chart12";
            series42.ChartArea = "ChartArea1";
            series42.Legend = "Legend1";
            series42.Name = "Series1";
            this.chart12.Series.Add(series42);
            this.chart12.Size = new System.Drawing.Size(1057, 162);
            this.chart12.TabIndex = 19;
            this.chart12.Text = "chart12";
            // 
            // chart13
            // 
            chartArea43.Name = "ChartArea1";
            this.chart13.ChartAreas.Add(chartArea43);
            legend43.Name = "Legend1";
            this.chart13.Legends.Add(legend43);
            this.chart13.Location = new System.Drawing.Point(178, 199);
            this.chart13.Name = "chart13";
            series43.ChartArea = "ChartArea1";
            series43.Legend = "Legend1";
            series43.Name = "Series1";
            this.chart13.Series.Add(series43);
            this.chart13.Size = new System.Drawing.Size(1057, 162);
            this.chart13.TabIndex = 20;
            this.chart13.Text = "chart13";
            // 
            // chart14
            // 
            chartArea44.Name = "ChartArea1";
            this.chart14.ChartAreas.Add(chartArea44);
            legend44.Name = "Legend1";
            this.chart14.Legends.Add(legend44);
            this.chart14.Location = new System.Drawing.Point(178, 384);
            this.chart14.Name = "chart14";
            series44.ChartArea = "ChartArea1";
            series44.Legend = "Legend1";
            series44.Name = "Series1";
            this.chart14.Series.Add(series44);
            this.chart14.Size = new System.Drawing.Size(1057, 162);
            this.chart14.TabIndex = 21;
            this.chart14.Text = "chart14";
            // 
            // chart15
            // 
            chartArea45.Name = "ChartArea1";
            this.chart15.ChartAreas.Add(chartArea45);
            legend45.Name = "Legend1";
            this.chart15.Legends.Add(legend45);
            this.chart15.Location = new System.Drawing.Point(178, 12);
            this.chart15.Name = "chart15";
            series45.ChartArea = "ChartArea1";
            series45.Legend = "Legend1";
            series45.Name = "Series1";
            this.chart15.Series.Add(series45);
            this.chart15.Size = new System.Drawing.Size(1057, 533);
            this.chart15.TabIndex = 22;
            this.chart15.Text = "chart15";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1247, 604);
            this.Controls.Add(this.chart15);
            this.Controls.Add(this.chart14);
            this.Controls.Add(this.chart13);
            this.Controls.Add(this.chart12);
            this.Controls.Add(this.chart11);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.chart10);
            this.Controls.Add(this.chart9);
            this.Controls.Add(this.chart8);
            this.Controls.Add(this.chart7);
            this.Controls.Add(this.chart6);
            this.Controls.Add(this.chart5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart4);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "AMEG graphics";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart15)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart9;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart11;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart12;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart14;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart15;
    }
}

