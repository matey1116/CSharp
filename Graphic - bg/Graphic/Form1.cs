﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.Linq;
using System.Drawing.Drawing2D;

namespace Graphic
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.chart1.MouseEnter += new EventHandler(chart1_MouseEnter);
            this.chart1.MouseWheel += new MouseEventHandler(chart1_MouseWheel);
            this.chart1.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            this.chart1.MouseLeave += new EventHandler(chart1_MouseLeave);
            chart15.PostPaint += new EventHandler<ChartPaintEventArgs>(chart1_PostPaint);

            chart1.Visible = false;
            chart2.Visible = false;
            chart3.Visible = false;
            chart4.Visible = false;
            chart5.Visible = false;
            chart6.Visible = false;
            chart7.Visible = false;
            chart8.Visible = false;
            chart9.Visible = false;
            chart10.Visible = false;
            chart11.Visible = false;
            chart12.Visible = false;
            chart13.Visible = false;
            chart14.Visible = false;
            chart15.Visible = false;
        }

        public class GlobalVariables
        {
            public static int m = 3200;
            public static int fs = 1063;
            public static double discret;
            public static double[] b = { 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05 };
            public static double[] a = { 1 };

            public static List<double> R = new List<double>();
            public static List<double> L = new List<double>();
            public static List<double> C1 = new List<double>();
            public static List<double> C2 = new List<double>();
            public static List<double> C3 = new List<double>();
            public static List<double> C4 = new List<double>();
            public static List<double> C5 = new List<double>();
            public static List<double> C6 = new List<double>();
            public static List<double> DL = new List<double>();
            public static List<double> DL2 = new List<double>();

            public static List<double> LwithoutNoise = new List<double>();


            public static IDictionary<double, double> Rdict = new Dictionary<double, double>();
            public static IDictionary<double, double> Ldict = new Dictionary<double, double>();
            public static IDictionary<double, double> C1dict = new Dictionary<double, double>();
            public static IDictionary<double, double> C2dict = new Dictionary<double, double>();
            public static IDictionary<double, double> C3dict = new Dictionary<double, double>();
            public static IDictionary<double, double> C4dict = new Dictionary<double, double>();
            public static IDictionary<double, double> C5dict = new Dictionary<double, double>();
            public static IDictionary<double, double> C6dict = new Dictionary<double, double>();

            public static IDictionary<double, double> DLdict = new Dictionary<double, double>();

            public static IDictionary<double, double> LCutdict = new Dictionary<double, double>();
            public static IDictionary<double, double> DLCutdict = new Dictionary<double, double>();

            public static IDictionary<double, double> temp = new Dictionary<double, double>();



        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        protected bool IsDigitsOnly(string[] str)
        {
            Regex regex = new Regex(@"^(-?\d*\.?\d*\t)*(-?\d*\.?\d*)$");
            for (int i = 0; i < str.Length; i++)
            {
                if (!regex.IsMatch(str[i]))
                {
                    return false;
                }
            }
            return true;
        }

        protected bool TextParse(string filename)
        {
            if (!IsDigitsOnly(File.ReadAllLines(filename)))
            {
                MessageBox.Show("Изберете валиден файл", "Грешен файл", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            StreamReader file = new StreamReader(filename);
            string line = file.ReadLine();
            if (line.Split('\t').Length == 8)
            {
                while (line != null)
                    try
                    {
                        String[] parms = line.Trim().Split('\t');

                        GlobalVariables.R.Add(double.Parse(parms[0], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.L.Add(double.Parse(parms[1], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C1.Add(double.Parse(parms[2], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C2.Add(double.Parse(parms[3], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C3.Add(double.Parse(parms[4], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C4.Add(double.Parse(parms[5], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C5.Add(double.Parse(parms[6], CultureInfo.InvariantCulture) * 0.00488);
                        GlobalVariables.C6.Add(double.Parse(parms[7], CultureInfo.InvariantCulture) * 0.00488);
                        line = file.ReadLine();
                    }
                    catch { }
                GlobalVariables.L = new List<double>(filter(GlobalVariables.b, GlobalVariables.a, GlobalVariables.L));

                for (double x = 0; x < Convert.ToDouble(GlobalVariables.m) / GlobalVariables.fs - (GlobalVariables.discret * 10); x = x + GlobalVariables.discret)
                {
                    GlobalVariables.Rdict.Add(new KeyValuePair<double, double>(x, GlobalVariables.R[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.Ldict.Add(new KeyValuePair<double, double>(x, GlobalVariables.L[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C1dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C1[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C2dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C2[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C3dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C3[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C4dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C4[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C5dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C5[Convert.ToInt32(x * GlobalVariables.fs)]));
                    GlobalVariables.C6dict.Add(new KeyValuePair<double, double>(x, GlobalVariables.C6[Convert.ToInt32(x * GlobalVariables.fs)]));

                    double DL = (GlobalVariables.L[(int)(x * GlobalVariables.fs) + 10] - GlobalVariables.L[(int)(x * GlobalVariables.fs)]) / 10;
                    GlobalVariables.DLdict.Add(new KeyValuePair<double, double>(x, DL));

                    if (x >= 2.2 && x <= 3.2)
                    {
                        GlobalVariables.LCutdict.Add(new KeyValuePair<double, double>(x, GlobalVariables.L[Convert.ToInt32(x * GlobalVariables.fs)]));
                        GlobalVariables.DLCutdict.Add(new KeyValuePair<double, double>(x, DL));
                    }
                }
            }
            else if (line.Split('\t').Length == 1)
            {
                if (File.ReadAllLines(filename).Count() < 3200)
                {
                    GlobalVariables.m = File.ReadLines(filename).Count();
                }

                while (line != null)
                    try
                    {
                        String[] parms = line.Trim().Split('\t');

                        GlobalVariables.L.Add(double.Parse(parms[0], CultureInfo.InvariantCulture));
                        line = file.ReadLine();
                    }
                    catch { }
                GlobalVariables.L = new List<double>(filter(GlobalVariables.b, GlobalVariables.a, GlobalVariables.L));
                for (double x = 0; x < Convert.ToDouble(GlobalVariables.m) * GlobalVariables.discret - (GlobalVariables.discret * 10); x = x + GlobalVariables.discret)
                {
                    GlobalVariables.Ldict.Add(new KeyValuePair<double, double>(x, GlobalVariables.L[Convert.ToInt32(x * GlobalVariables.fs)]));
                    double DL = (GlobalVariables.L[(int)(x * GlobalVariables.fs) + 10] - GlobalVariables.L[(int)(x * GlobalVariables.fs)]) / 10;
                    GlobalVariables.DLdict.Add(new KeyValuePair<double, double>(x, DL));
                    if (x >= GlobalVariables.m * GlobalVariables.discret * 0.34375 && x <= GlobalVariables.m * GlobalVariables.discret * 0.5)
                    {
                        GlobalVariables.LCutdict.Add(new KeyValuePair<double, double>(x, GlobalVariables.L[Convert.ToInt32(x * GlobalVariables.fs)]));
                        GlobalVariables.DLCutdict.Add(new KeyValuePair<double, double>(x, DL));
                    }
                }

            }


            return true;
        }

        protected void Plot(string X, string Y, Chart chart1, IDictionary<double, double> dict)
        {

            chart1.Visible = true;
            if (chart1.Series.IsUniqueName("Graph"))
            {
                chart1.Series.Add("Graph");
            }
            chart1.Series[0].ChartType = SeriesChartType.Line;
            chart1.DataSource = dict;
            chart1.Series[0].XValueMember = "Key";
            chart1.Series[0].YValueMembers = "Value";
            chart1.Series[0].IsVisibleInLegend = false;
            chart1.Series[0].Color = Color.Red;

            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "0.##";
            chart1.ChartAreas[0].AxisY.LabelStyle.Format = "0.##";
            chart1.ChartAreas[0].AxisX.Title = X;

            chart1.ChartAreas[0].AxisY.Title = Y;

            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;

            chart1.Legends.Clear();
            chart1.ChartAreas[0].Position = new ElementPosition(0, 0, 100, 100);


            chart1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (GlobalVariables.R.Count != 0)
            {
                if (chart1.DataSource == GlobalVariables.Rdict && chart1.Visible == true)
                {
                    Plot("време, s", "C3, mV", chart1, GlobalVariables.C3dict); //chart1
                    Plot("време, s", "C4, mV", chart2, GlobalVariables.C4dict); //chart2
                    Plot("време, s", "C5, mV", chart3, GlobalVariables.C5dict); //chart3
                    Plot("време, s", "C6, mV", chart4, GlobalVariables.C6dict); //chart4
                }
                else if (chart1.DataSource == GlobalVariables.C3dict && chart1.Visible == true)
                {
                    chart1.Visible = false;
                    chart2.Visible = false;
                    chart3.Visible = false;
                    chart4.Visible = false;
                    Plot("време, s", "UL, mV", chart5, GlobalVariables.LCutdict);           //chart5
                    Plot("време, s", "d(U_L)/dt , mV", chart6, GlobalVariables.DLCutdict); //chart6
                    Plot("време, s", "U_L, mV", chart7, GlobalVariables.Ldict);                 //chart7
                    Plot("време, s", "d(U_L)/dt , mV", chart8, GlobalVariables.DLdict);    //chart8

                }
                else if (chart5.Visible == true)
                {
                    chart5.Visible = false;
                    chart6.Visible = false;
                    chart7.Visible = false;
                    chart8.Visible = false;
                    //Plot("време, s", "L, mV", chart9, GlobalVariables.Ldict);
                    step3();
                    step4();
                }
                else if (chart9.Visible == true)
                {
                    chart9.Visible = false;
                    chart10.Visible = false;
                    noise();
                }
                else if (chart12.Visible == true)
                {
                    chart12.Visible = false;
                    chart13.Visible = false;
                    chart14.Visible = false;
                    fazagraph();
                }
                else if (chart11.Visible == true)
                {
                    chart11.Visible = false;
                    chart15.Visible = true;
                    chart15.Visible = true;

                    if (chart15.Series.IsUniqueName("Graph"))
                    {
                        chart15.Series.Add("Graph");
                    }
                    prepare3dChart(chart15, chart15.ChartAreas[0]);
                }
            }
            else
            {
                if (chart5.Visible == true)
                {
                    chart5.Visible = false;
                    chart6.Visible = false;
                    chart7.Visible = false;
                    chart8.Visible = false;
                    step3();
                    step4();
                }
                else if (chart9.Visible == true)
                {
                    chart9.Visible = false;
                    chart10.Visible = false;
                    noise();
                }
                else if (chart12.Visible == true)
                {
                    chart12.Visible = false;
                    chart13.Visible = false;
                    chart14.Visible = false;
                    fazagraph();
                }
                else if (chart11.Visible == true)
                {
                    chart11.Visible = false;
                    chart15.Visible = true;
                    if (chart15.Series.IsUniqueName("Graph"))
                    {
                        chart15.Series.Add("Graph");
                    }
                    prepare3dChart(chart15, chart15.ChartAreas[0]);
                }
            }

        }

        public void step3()
        {
            Plot("време, s", "U_L, mV, M=0.016%", chart9, GlobalVariables.Ldict);
            double M = 1.6;
            double Mp = (M * 0.016) / 100;
            int beg = 0;
            int j = 0;

            IDictionary<double, double> dict = new Dictionary<double, double>();

            for (int i = 20; i < GlobalVariables.m - 20; i++)
            {
                double D1 = GlobalVariables.L[i] - GlobalVariables.L[i - 20];
                double D2 = GlobalVariables.L[i] - GlobalVariables.L[i + 20];

                double peak = Math.Abs(D1) + Math.Abs(D2);
                if (peak > M && (i - beg) > 200)
                {
                    M = peak;
                    Mp = M * 0.016 / 100;

                    var pt = chart9.Series[0].Points[i];

                    pt.MarkerColor = Color.Black;
                    pt.MarkerSize = 5;
                    pt.MarkerStyle = MarkerStyle.Circle;

                    j = j + 1;
                    beg = i;
                }

                if ((i - beg) > 200 && (i - beg) < 1000 && M > 0.2)
                {
                    M = M - Mp;
                }
                else if ((i - beg) > 1000 && M > 0.2)
                {
                    M = M - (Mp / 2);
                }

            }
        }

        public void step4()
        {
            Plot("време, s", "U_L, mV, M=0.030%", chart10, GlobalVariables.Ldict);
            double M = 3;
            double Mp = (M * 0.03) / 100;
            int beg = 0;
            int j = 0;

            IDictionary<double, double> dict = new Dictionary<double, double>();

            for (int i = 0; i < GlobalVariables.m - 40; i++)
            {
                double D1 = GlobalVariables.L[i + 20] - GlobalVariables.L[i];
                double D2 = GlobalVariables.L[i + 20] - GlobalVariables.L[i + 40];

                double peak = Math.Abs(D1) + Math.Abs(D2);
                if (peak > M && (i - beg) > 200)
                {
                    M = peak;
                    Mp = M * 0.03 / 100;

                    var pt = chart10.Series[0].Points[i];

                    pt.MarkerColor = Color.Black;
                    pt.MarkerSize = 5;
                    pt.MarkerStyle = MarkerStyle.Circle;

                    j = j + 1;
                    beg = i;
                }

                if ((i - beg) > 200 && (i - beg) < 1000 && M > 0.2)
                {
                    M = M - Mp;
                }
                else if ((i - beg) > 1000 && M > 0.2)
                {
                    M = M - (Mp / 2);
                }

            }
        }

        public void clear()
        {
            //clear all variables to parse again
            GlobalVariables.m = 3200;
            GlobalVariables.L.Clear();
            GlobalVariables.R.Clear();
            GlobalVariables.C1.Clear();
            GlobalVariables.C2.Clear();
            GlobalVariables.C3.Clear();
            GlobalVariables.C4.Clear();
            GlobalVariables.C5.Clear();
            GlobalVariables.C6.Clear();
            GlobalVariables.DL.Clear();
            GlobalVariables.DL2.Clear();
            GlobalVariables.LwithoutNoise.Clear();


            GlobalVariables.Ldict.Clear();
            GlobalVariables.Rdict.Clear();
            GlobalVariables.C1dict.Clear();
            GlobalVariables.C2dict.Clear();
            GlobalVariables.C3dict.Clear();
            GlobalVariables.C4dict.Clear();
            GlobalVariables.C5dict.Clear();
            GlobalVariables.C6dict.Clear();

            GlobalVariables.DLdict.Clear();
            GlobalVariables.LCutdict.Clear();
            GlobalVariables.DLCutdict.Clear();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.  
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "All Files (*.*)|*.*";
            openFileDialog1.Title = "Select a Data File";

            // Show the Dialog.  
            // If the user clicked OK in the dialog and  
            // a .CUR file was selected, open it.  
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Assign the cursor in the Stream to the Form's Cursor property.  
                string sFileName = openFileDialog1.FileName;
                //clear variables before new parse
                clear();

                chart1.Visible = false;
                chart2.Visible = false;
                chart3.Visible = false;
                chart4.Visible = false;
                chart5.Visible = false;
                chart6.Visible = false;
                chart7.Visible = false;
                chart8.Visible = false;
                chart9.Visible = false;
                chart10.Visible = false;
                chart11.Visible = false;
                chart12.Visible = false;
                chart13.Visible = false;
                chart14.Visible = false;
                chart15.Visible = false;

                GlobalVariables.discret = Convert.ToDouble(1) / GlobalVariables.fs;

                //parse data and display charts
                if (TextParse(sFileName))
                {
                    if (GlobalVariables.R.Count != 0)
                    {
                        Plot("време, s", "R, mV", chart1, GlobalVariables.Rdict);
                        Plot("време, s", "L, mV", chart2, GlobalVariables.Ldict);
                        Plot("време, s", "C1, mV", chart3, GlobalVariables.C1dict);
                        Plot("време, s", "C2, mV", chart4, GlobalVariables.C2dict);
                    }
                    else
                    {
                        Plot("време, s", "UL, mV", chart5, GlobalVariables.LCutdict);           //chart5
                        Plot("време, s", "d(U_L)/dt , mV", chart6, GlobalVariables.DLCutdict); //chart6
                        Plot("време, s", "U_L, mV", chart7, GlobalVariables.Ldict);                 //chart7
                        Plot("време, s", "d(U_L)/dt , mV", chart8, GlobalVariables.DLdict);    //chart8
                    }
                }
                //label1.Text = "" + GlobalVariables.Ldict.Count();
            }
        }

        public static double[] getRealArrayScalarDiv(double[] dDividend, double dDivisor)
        {
            if (dDividend == null)
            {
                throw new ArgumentException("The array must be defined or diferent to null");
            }
            if (dDividend.Length == 0)
            {
                throw new ArgumentException("The size array must be greater than Zero");
            }
            double[] dQuotient = new double[dDividend.Length];

            for (int i = 0; i < dDividend.Length; i++)
            {
                if (!(dDivisor == 0.0))
                {
                    dQuotient[i] = dDividend[i] / dDivisor;
                }
                else
                {
                    if (dDividend[i] > 0.0)
                    {
                        dQuotient[i] = Double.PositiveInfinity;
                    }
                    if (dDividend[i] == 0.0)
                    {
                        dQuotient[i] = Double.NaN;
                    }
                    if (dDividend[i] < 0.0)
                    {
                        dQuotient[i] = Double.NegativeInfinity;
                    }
                }
            }
            return dQuotient;
        }

        public static double[] filter(double[] b, double[] a, List<double> x)
        {
            double[] filter = null;
            double[] a1 = getRealArrayScalarDiv(a, a[0]);
            double[] b1 = getRealArrayScalarDiv(b, a[0]);
            int sx = x.Count;
            filter = new double[sx];
            filter[0] = b1[0] * x[0];
            for (int i = 1; i < sx; i++)
            {
                filter[i] = 0.0;
                for (int j = 0; j <= i; j++)
                {
                    int k = i - j;
                    if (j > 0)
                    {
                        if ((k < b1.Length) && (j < x.Count))
                        {
                            filter[i] += b1[k] * x[j];
                        }
                        if ((k < filter.Length) && (j < a1.Length))
                        {
                            filter[i] -= a1[j] * filter[k];
                        }
                    }
                    else
                    {
                        if ((k < b1.Length) && (j < x.Count))
                        {
                            filter[i] += (b1[k] * x[j]);
                        }
                    }
                }
            }
            return filter;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frm = Form.ActiveForm;
            using (var bmp = new Bitmap(frm.Width, frm.Height))
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                bmp.Save(unixTimestamp + ".jpg", ImageFormat.Jpeg);
            }
        }

        private void fazagraph()
        {
            chart11.Visible = true;
            if (chart11.Series.IsUniqueName("Graph"))
            {
                chart11.Series.Add("Graph");
            }
            chart11.Series[0].ChartType = SeriesChartType.Line;
            chart11.Series[0].XValueMember = "Key";
            chart11.Series[0].YValueMembers = "Value";
            chart11.Series[0].IsVisibleInLegend = false;
            chart11.Series[0].Color = Color.Red;

            chart11.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart11.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart11.ChartAreas[0].AxisX.LabelStyle.Format = "0.##";
            chart11.ChartAreas[0].AxisY.LabelStyle.Format = "0.##";

            chart11.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart11.ChartAreas[0].AxisY.ScaleView.Zoomable = true;

            chart11.Legends.Clear();
            chart11.ChartAreas[0].Position = new ElementPosition(0, 0, 100, 100);


            for (int i = 0; i < GlobalVariables.L.Count() - 10; i++)
            {
                int test = GlobalVariables.L.Count();
                double DL = (GlobalVariables.L[i + 10] - GlobalVariables.L[i]) / 10;
                GlobalVariables.DL.Add(DL);
                chart11.Series[0].Points.AddXY(GlobalVariables.L[i], DL);
            }
            for (int i = 0; i < GlobalVariables.DL.Count() - 10; i++)
            {
                double DL2 = (GlobalVariables.DL[i + 10] - GlobalVariables.DL[i]) / 10;
                GlobalVariables.DL2.Add(DL2);
            }
            chart11.ChartAreas[0].AxisX.Title = "U_L, mV";
            chart11.ChartAreas[0].AxisY.Title = "d(U_L)/dt";

            chart11.Refresh();
        }

        private void noise()
        {
            IDictionary<double, double> dict = new Dictionary<double, double>();
            IDictionary<double, double> dirt = new Dictionary<double, double>();
            double temp1 = 0, temp2 = 0;
            for (int i = 5; i < (GlobalVariables.m - 5); i++)
            {
                double final;

                temp1 = GlobalVariables.L[i - 5] + GlobalVariables.L[i - 4] + GlobalVariables.L[i - 3] +
                    GlobalVariables.L[i - 2] + GlobalVariables.L[i - 1] + GlobalVariables.L[i] +
                    GlobalVariables.L[i + 1] + GlobalVariables.L[i + 2] + GlobalVariables.L[i + 3] + GlobalVariables.L[i + 4];

                temp1 = temp1 / 10;
                temp2 = GlobalVariables.L[i + 5] - GlobalVariables.L[i - 5];
                temp2 = temp2 / 20;
                final = temp1 - temp2;

                GlobalVariables.LwithoutNoise.Add(final);

                dict.Add(new KeyValuePair<double, double>(i, final));

            }
            Plot("време, s", "U_L със шум, mV", chart12, GlobalVariables.Ldict);
            Plot("време, s", "U_L без шум, mV", chart13, dict);

            for (int i = 5; i < GlobalVariables.m - 5; i++)
            {
                double temp = GlobalVariables.L[i] - dict[i];
                dirt.Add(new KeyValuePair<double, double>(i - 5, temp));
            }
            Plot("време, s", "Un шум, mV", chart14, dirt);

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (GlobalVariables.R.Count != 0)
            {
                if (chart15.Visible == true)
                {
                    chart15.Visible = false;
                    fazagraph();
                }
                else if (chart11.Visible == true)
                {
                    chart11.Visible = false;
                    noise();
                }
                else if (chart14.Visible == true)
                {
                    chart14.Visible = false;
                    chart13.Visible = false;
                    chart12.Visible = false;

                    step3();
                    step4();
                }
                else if (chart9.Visible == true)
                {
                    chart9.Visible = false;
                    chart10.Visible = false;
                    Plot("време, s", "UL, mV", chart5, GlobalVariables.LCutdict);           //chart5
                    Plot("време, s", "d(U_L)/dt , mV", chart6, GlobalVariables.DLCutdict); //chart6
                    Plot("време, s", "U_L, mV", chart7, GlobalVariables.Ldict);                 //chart7
                    Plot("време, s", "d(U_L)/dt , mV", chart8, GlobalVariables.DLdict);    //chart8

                }
                else if (chart5.Visible == true)
                {
                    chart5.Visible = false;
                    chart6.Visible = false;
                    chart7.Visible = false;
                    chart8.Visible = false;
                    Plot("време, s", "C3, mV", chart1, GlobalVariables.C3dict); //chart1
                    Plot("време, s", "C4, mV", chart2, GlobalVariables.C4dict); //chart2
                    Plot("време, s", "C5, mV", chart3, GlobalVariables.C5dict); //chart3
                    Plot("време, s", "C6, mV", chart4, GlobalVariables.C6dict); //chart4
                }
                else if (chart1.Visible == true && chart1.DataSource == GlobalVariables.C3dict)
                {
                    Plot("време, s", "R, mV", chart1, GlobalVariables.Rdict);
                    Plot("време, s", "L, mV", chart2, GlobalVariables.Ldict);
                    Plot("време, s", "C1, mV", chart3, GlobalVariables.C1dict);
                    Plot("време, s", "C2, mV", chart4, GlobalVariables.C2dict);
                }
            }
            else
            {
                if (chart15.Visible == true)
                {
                    chart15.Visible = false;
                    fazagraph();
                }
                else if (chart11.Visible == true)
                {
                    chart11.Visible = false;
                    noise();
                }
                else if (chart14.Visible == true)
                {
                    chart14.Visible = false;
                    chart13.Visible = false;
                    chart12.Visible = false;

                    step3();
                    step4();
                }
                else if (chart9.Visible == true)
                {
                    chart9.Visible = false;
                    chart10.Visible = false;
                    Plot("", "UL, mV", chart5, GlobalVariables.LCutdict);           //chart5
                    Plot("time, s", "d(U_L)/dt , mV", chart6, GlobalVariables.DLCutdict); //chart6
                    Plot("time, s", "U_L, mV", chart7, GlobalVariables.Ldict);                 //chart7
                    Plot("time, s", "d(U_L)/dt , mV", chart8, GlobalVariables.DLdict);    //chart8

                }
            }
        }
        void chart1_MouseEnter(object sender, EventArgs e)
        {
            if (!chart1.Focused)
                chart1.Focus();
        }
        private void chart1_MouseWheel(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Delta < 0)
                {
                    chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
                    chart1.ChartAreas[0].AxisY.ScaleView.ZoomReset();
                }

                if (e.Delta > 0)
                {
                    double xMin = chart1.ChartAreas[0].AxisX.ScaleView.ViewMinimum;
                    double xMax = chart1.ChartAreas[0].AxisX.ScaleView.ViewMaximum;
                    double yMin = chart1.ChartAreas[0].AxisY.ScaleView.ViewMinimum;
                    double yMax = chart1.ChartAreas[0].AxisY.ScaleView.ViewMaximum;

                    double posXStart = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) - (xMax - xMin) / 4;
                    double posXFinish = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) + (xMax - xMin) / 4;
                    double posYStart = chart1.ChartAreas[0].AxisY.PixelPositionToValue(e.Location.Y) - (yMax - yMin) / 4;
                    double posYFinish = chart1.ChartAreas[0].AxisY.PixelPositionToValue(e.Location.Y) + (yMax - yMin) / 4;

                    chart1.ChartAreas[0].AxisX.ScaleView.Zoom(posXStart, posXFinish);
                    chart1.ChartAreas[0].AxisY.ScaleView.Zoom(posYStart, posYFinish);



                }
            }
            catch { }
        }

        private void chart_GetToolTipText(object sender, ToolTipEventArgs e)
        {

            // If the mouse isn't on the plotting area, a datapoint, or gridline then exit
            HitTestResult htr = chart1.HitTest(e.X, e.Y);
            if (htr.ChartElementType != ChartElementType.PlottingArea && htr.ChartElementType != ChartElementType.DataPoint && htr.ChartElementType != ChartElementType.Gridlines)
                return;

            ChartArea ca = chart1.ChartAreas[0]; // Assuming you only have 1 chart area on the chart

            double xCoord = ca.AxisX.PixelPositionToValue(e.X);
            double yCoord = ca.AxisY.PixelPositionToValue(e.Y);

            e.Text = "X = " + Math.Round(xCoord, 2).ToString() + "\nY = " + Math.Round(yCoord, 2).ToString();
        }
        void chart1_MouseLeave(object sender, EventArgs e)
        {
            if (chart1.Focused)
                chart1.Parent.Focus();
        }

        void prepare3dChart(Chart chart, ChartArea ca)
        {   
            ca.Area3DStyle.Enable3D = true;  // set the chartarea to 3D!
            ca.AxisX.Title = "U_L, mV";
            ca.AxisY.Title = "d(U_L)/dt, mV";
            ca.AxisX.MajorGrid.Interval = 250;
            ca.AxisY.MajorGrid.Interval = 250;
            ca.AxisX.MinorGrid.Enabled = true;
            ca.AxisY.MinorGrid.Enabled = true;
            ca.AxisX.MinorGrid.Interval = 50;
            ca.AxisY.MinorGrid.Interval = 50;
            ca.AxisX.MinorGrid.LineColor = Color.LightSlateGray;
            ca.AxisY.MinorGrid.LineColor = Color.LightSlateGray;

            // we add two series:
            chart.Series.Clear();

            Series s = chart.Series.Add("S");
            s.ChartType = SeriesChartType.Bubble;   // this ChartType has a YValue array
            s.MarkerStyle = MarkerStyle.Circle;
            s["PixelPointWidth"] = "100";
            s["PixelPointGapDepth"] = "1";

            chart.ApplyPaletteColors();

            addTestData(chart);
        }

        void addTestData(Chart chart)
        {
            try
            {
                Random rnd = new Random(9);
                //for (int i = 0; i < GlobalVariables.DL2.Count() -1; i++)
                //{
                //    double x = GlobalVariables.L[i];
                //    double y = GlobalVariables.DL[i];
                //    double z = GlobalVariables.DL2[i];

                //    AddXY3d(chart.Series[0], x, y, z);
                //}
                int i = 0;
                while (i < GlobalVariables.DL2.Count() - 100)
                {
                    double x = GlobalVariables.L[i];
                    double y = GlobalVariables.DL[i];
                    double z = GlobalVariables.DL2[i];

                    AddXY3d(chart.Series[0], x, y, z);
                    i++;
                }
            }
            catch (ArgumentOutOfRangeException e)
            {

            }
        }

        int AddXY3d(Series s, double xVal, double yVal, double zVal)
        {
            int p = s.Points.AddXY(xVal, yVal, zVal);
            // the DataPoint are transparent to the regular chart drawing:
            s.Points[p].Color = Color.Transparent;
            return p;
        }

        private void chart1_PostPaint(object sender, ChartPaintEventArgs e)
        {
            Chart chart = sender as Chart;

            if (chart.Series.Count < 1) return;
            if (chart.Series[0].Points.Count < 1) return;

            ChartArea ca = chart.ChartAreas[0];
            e.ChartGraphics.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            List<List<PointF>> data = new List<List<PointF>>();
            foreach (Series s in chart.Series)
                data.Add(GetPointsFrom3D(ca, s, s.Points.ToList(), e.ChartGraphics));

            renderLines(data, e.ChartGraphics.Graphics, chart, true);

        }

        List<PointF> GetPointsFrom3D(ChartArea ca, Series s,
                             List<DataPoint> dPoints, ChartGraphics cg)
        {
            var p3t = dPoints.Select(x => new Point3D((float)ca.AxisX.ValueToPosition(x.XValue),
                (float)ca.AxisY.ValueToPosition(x.YValues[0]),
                (float)ca.AxisY.ValueToPosition(x.YValues[1]))).ToArray();
            ca.TransformPoints(p3t.ToArray());

            return p3t.Select(x => cg.GetAbsolutePoint(new PointF(x.X, x.Y))).ToList();
        }

        void renderLines(List<List<PointF>> data, Graphics graphics, Chart chart, bool curves)
        {
            for (int i = 0; i < chart.Series.Count; i++)
            {
                if (data[i].Count > 1)
                    using (Pen pen = new Pen(Color.FromArgb(64, chart.Series[i].Color), 2.5f))
                        if (curves) graphics.DrawCurve(pen, data[i].ToArray());
                        else graphics.DrawLines(pen, data[i].ToArray());
            }
        }
    }
}
