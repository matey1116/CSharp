﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ECG
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            test();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            mom_grafika.Form1 form = new mom_grafika.Form1();
            Hide();
            form.Closed += (s, args) => formClosed();
            form.Show();
        }

        private void formClosed()
        {
            foreach (TextBox tb in this.Controls.OfType<AutoCompleteTextBox>())
            {
                tb.Text = "";
            }
            Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            this.Size = new Size(this.Width, 465);
            AutoCompleteTextBox tb = new AutoCompleteTextBox();
            tb.Values = vars.array;
            tb.Font = new Font("Arial", 12, FontStyle.Bold);
            tb.Location = new Point(12, 129);
            tb.Size = new Size(281, 26);
            tb.TextChanged += new EventHandler(tb_textchanged);
            this.Controls.Add(tb);
            tb.Select();
        }

        public class vars
        {
            public static AutoCompleteStringCollection col1 = new AutoCompleteStringCollection();
            public static AutoCompleteStringCollection col3 = new AutoCompleteStringCollection();
            public static string[] array;

            public static List<string> str = new List<string>();
            public static List<double> d1 = new List<double>();
            public static List<double> d2 = new List<double>();
            public static List<double> d3 = new List<double>();
            public static List<double> d4 = new List<double>();
            public static List<double> d5 = new List<double>();
            public static List<double> d6 = new List<double>();
            public static List<double> d7 = new List<double>();
            public static List<double> d8 = new List<double>();
            public static List<double> d9 = new List<double>();
            public static List<double> d10 = new List<double>();
            public static List<double> d11 = new List<double>();
            public static List<double> d12 = new List<double>();
            public static List<double> d13 = new List<double>();

            public static int length;

        }

        protected void clear()
        {
            vars.str.Clear();
            vars.d1.Clear();
            vars.d2.Clear();
            vars.d3.Clear();
            vars.d4.Clear();
            vars.d5.Clear();
            vars.d6.Clear();
            vars.d7.Clear();
            vars.d8.Clear();
            vars.d9.Clear();
            vars.d10.Clear();
            vars.d11.Clear();
            vars.d12.Clear();
            vars.d13.Clear();
        }


        private void test()
        {
            DirectoryInfo dinfo = new DirectoryInfo(Application.StartupPath);
            FileInfo[] Files = dinfo.GetFiles("*.txt");
            foreach (FileInfo file in Files)
            {
                TextParse(Application.StartupPath + '\u005c' + file.Name);
            }
        }

        protected void TextParse(string filename)
        {
            vars.length = File.ReadLines(filename).Count();
            //clear();
            StreamReader file = new StreamReader(filename);
            string line = file.ReadLine();
            while (line != null)
                try
                {
                    String[] parms = line.Trim().Split('\t');
                    if (!vars.str.Contains(parms[0]))
                    {
                        vars.str.Add(parms[0]);
                        vars.d1.Add(double.Parse(parms[1], CultureInfo.InvariantCulture));
                        vars.d2.Add(double.Parse(parms[2], CultureInfo.InvariantCulture));
                        vars.d3.Add(double.Parse(parms[3], CultureInfo.InvariantCulture));
                        vars.d4.Add(double.Parse(parms[4], CultureInfo.InvariantCulture));
                        vars.d5.Add(double.Parse(parms[5], CultureInfo.InvariantCulture));
                        vars.d6.Add(double.Parse(parms[6], CultureInfo.InvariantCulture));
                        vars.d7.Add(double.Parse(parms[7], CultureInfo.InvariantCulture));
                        vars.d8.Add(double.Parse(parms[8], CultureInfo.InvariantCulture));
                        vars.d9.Add(double.Parse(parms[9], CultureInfo.InvariantCulture));
                        vars.d10.Add(double.Parse(parms[10], CultureInfo.InvariantCulture));
                        vars.d11.Add(double.Parse(parms[11], CultureInfo.InvariantCulture));
                        vars.d12.Add(double.Parse(parms[12], CultureInfo.InvariantCulture));
                    }


                    line = file.ReadLine();
                }
                catch { }

            vars.array = vars.str.ToArray();
        }

        private void tb_textchanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (Regex.IsMatch(tb.Text, @"[A-Z][A-Z]-[0-9][0-9]-[A-Z][A-Z]-[MF]"))
            {
                int index = vars.str.IndexOf(tb.Text);
                mom_grafika.Form1 form = new mom_grafika.Form1(
                    vars.str[index], vars.d1[index], vars.d2[index],
                    vars.d3[index], vars.d4[index], vars.d5[index],
                    vars.d6[index], vars.d7[index], vars.d8[index],
                    vars.d9[index], vars.d10[index], vars.d11[index], vars.d12[index]);
                Hide();
                tb.Text = "";
                form.Closed += (s, args) => Show();
                form.Show();
            }
        }
    }
}

public class AutoCompleteTextBox : TextBox
{

    private ListBox _listBox;
    private bool _isAdded;
    private String[] _values;
    private String _formerValue = String.Empty;

    public AutoCompleteTextBox()
    {
        InitializeComponent();
        ResetListBox();
    }

    private void InitializeComponent()
    {
        _listBox = new ListBox();
        this.KeyDown += this_KeyDown;
        this.KeyUp += this_KeyUp;
    }

    private void ShowListBox()
    {
        if (!_isAdded)
        {
            Parent.Controls.Add(_listBox);
            _listBox.Left = Left;
            _listBox.Top = Top + Height;
            _isAdded = true;
        }
        _listBox.Visible = true;
        _listBox.BringToFront();
    }

    private void ResetListBox()
    {
        _listBox.Visible = false;
    }

    private void this_KeyUp(object sender, KeyEventArgs e)
    {
        UpdateListBox();
    }

    private void this_KeyDown(object sender, KeyEventArgs e)
    {
        switch (e.KeyCode)
        {
            case Keys.Enter:
            case Keys.Tab:
                {
                    if (_listBox.Visible)
                    {
                        Text = _listBox.SelectedItem.ToString();
                        ResetListBox();
                        _formerValue = Text;
                        this.Select(this.Text.Length, 0);
                        e.Handled = true;
                    }
                    break;
                }
            case Keys.Down:
                {
                    if ((_listBox.Visible) && (_listBox.SelectedIndex < _listBox.Items.Count - 1))
                        _listBox.SelectedIndex++;
                    e.Handled = true;
                    break;
                }
            case Keys.Up:
                {
                    if ((_listBox.Visible) && (_listBox.SelectedIndex > 0))
                        _listBox.SelectedIndex--;
                    e.Handled = true;
                    break;
                }


        }
    }

    protected override bool IsInputKey(Keys keyData)
    {
        switch (keyData)
        {
            case Keys.Tab:
                if (_listBox.Visible)
                    return true;
                else
                    return false;
            default:
                return base.IsInputKey(keyData);
        }
    }

    private void UpdateListBox()
    {
        if (Text == _formerValue)
            return;

        _formerValue = this.Text;
        string word = this.Text;

        if (_values != null && word.Length > 0)
        {
            string[] matches = Array.FindAll(_values,
                x => (x.ToLower().Contains(word.ToLower())));
            if (matches.Length > 0)
            {
                ShowListBox();
                _listBox.BeginUpdate();
                _listBox.Items.Clear();
                Array.ForEach(matches, x => _listBox.Items.Add(x));
                _listBox.SelectedIndex = 0;
                _listBox.Height = 0;
                _listBox.Width = 0;
                Focus();
                using (Graphics graphics = _listBox.CreateGraphics())
                {
                    for (int i = 0; i < _listBox.Items.Count; i++)
                    {
                        if (i < 20)
                            _listBox.Height += _listBox.GetItemHeight(i);
                        // it item width is larger than the current one
                        // set it to the new max item width
                        // GetItemRectangle does not work for me
                        // we add a little extra space by using '_'
                        int itemWidth = (int)graphics.MeasureString(((string)_listBox.Items[i]) + "_", _listBox.Font).Width;
                        _listBox.Width = (_listBox.Width < itemWidth) ? itemWidth : this.Width; ;
                    }
                }
                _listBox.EndUpdate();
            }
            else
            {
                ResetListBox();
            }
        }
        else
        {
            ResetListBox();
        }
    }

    public String[] Values
    {
        get
        {
            return _values;
        }
        set
        {
            _values = value;
        }
    }

    public List<String> SelectedValues
    {
        get
        {
            String[] result = Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return new List<String>(result);
        }
    }
}
