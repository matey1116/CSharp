﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace mom_grafika
{
    public partial class Form1 : Form
    {
        IDictionary<double, double> dict = new Dictionary<double, double>();

        public Form1()
        {
            InitializeComponent();
            label37.Visible = false;
            label38.Visible = false;
            label39.Visible = false;
            label40.Visible = false;
            label41.Visible = false;
            label42.Visible = false;
            label43.Visible = false;
            label44.Visible = false;
            label45.Visible = false;
            label46.Visible = false;
            label48.Visible = false;
            label49.Visible = false;
            label50.Visible = false;
            label51.Visible = false;
            label52.Visible = false;
            label53.Visible = false;
            label55.Visible = false;
            label54.Visible = false;

            label56.Visible = false;
            label57.Visible = false;
            label58.Visible = false;
            label59.Visible = false;
            label60.Visible = false;
            label61.Visible = false;
            label62.Visible = false;
            label63.Visible = false;
            label64.Visible = false;


            this.chart1.MouseEnter += new EventHandler(chart1_MouseEnter);
            this.chart1.MouseWheel += new MouseEventHandler(chart1_MouseWheel);
            this.chart1.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(chart_GetToolTipText);
            this.chart1.MouseLeave += new EventHandler(chart1_MouseLeave);

            defaultVariables();

            chart1.Visible = false;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            image();

        }

        public Form1(string str, double d1, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12)
        {
            InitializeComponent();

            label37.Visible = false;
            label38.Visible = false;
            label39.Visible = false;
            label40.Visible = false;
            label41.Visible = false;
            label42.Visible = false;
            label43.Visible = false;
            label44.Visible = false;
            label45.Visible = false;
            label46.Visible = false;
            label48.Visible = false;
            label49.Visible = false;
            label50.Visible = false;
            label51.Visible = false;
            label52.Visible = false;
            label53.Visible = false;
            label55.Visible = false;
            label54.Visible = false;

            label56.Visible = false;
            label57.Visible = false;
            label58.Visible = false;
            label59.Visible = false;
            label60.Visible = false;
            label61.Visible = false;
            label62.Visible = false;
            label63.Visible = false;
            label64.Visible = false;


            this.chart1.MouseEnter += new EventHandler(chart1_MouseEnter);
            this.chart1.MouseWheel += new MouseEventHandler(chart1_MouseWheel);
            this.chart1.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(chart_GetToolTipText);
            this.chart1.MouseLeave += new EventHandler(chart1_MouseLeave);


            textBox1.Text = Convert.ToString(d1);
            textBox2.Text = "0.23";
            textBox3.Text = Convert.ToString(d3);
            textBox4.Text = Convert.ToString(d4);
            textBox5.Text = "0.027";
            textBox6.Text = Convert.ToString(0.4*d6);
            //textBox7.Text = "0.17";
            textBox8.Text = "1.5";
            textBox9.Text = Convert.ToString(d6);
            textBox10.Text = "0.24";
            textBox11.Text = Convert.ToString(0.35*d6);
            textBox12.Text = Convert.ToString(d7);
            textBox13.Text = "0.35";
            textBox14.Text = Convert.ToString(d8);
            textBox15.Text = Convert.ToString(d9);
            textBox16.Text = "0.035";
            textBox17.Text = Convert.ToString(d10*0.1);
            textBox18.Text = Convert.ToString(d10*0.9);

            GlobalVariables.name = str;

            chart1.Visible = false;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            image();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void image()
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            if (File.Exists("PL-grafics-ECG.jpg"))
            {
                Image image = Image.FromFile("PL-grafics-ECG.jpg");
                pictureBox1.Image = image;
            }
            else
            {
                pictureBox1.ImageLocation = "https://i.imgur.com/VdCYejT.jpg";
            }

        }

        public class GlobalVariables
        {
            public static double ptint;
            public static double li, a_pwav, d_pwav, t_pwav, a_qwav, d_qwav, t_qwav, a_qrswav, d_qrswav, a_swav, d_swav, t_swav, a_twav, d_twav, t_twav, a_uwav, d_uwav, t_uwav;
            public static double rate;
            public static double RR;
            public static double indexfcc, qtc, pqc, pqd, pqs;
            public static double SI;
            public static double fom;
            public static double fp;
            public static string name = "M";
        }

        public void button1_Click_1(object sender, EventArgs e)
        {

            if (label37.Visible == true)
            {
                label37.Visible = false;
                label38.Visible = false;
                label39.Visible = false;
                label40.Visible = false;
                label41.Visible = false;
                label42.Visible = false;
                label43.Visible = false;
                label44.Visible = false;
                label45.Visible = false;
                label46.Visible = false;
                label48.Visible = false;
                label49.Visible = false;
                label53.Visible = false;
                label50.Visible = false;
                label51.Visible = false;
                label52.Visible = false;

                label53.Visible = false;
                label54.Visible = false;
                label55.Visible = false;
                label56.Visible = false;
                label57.Visible = false;
                label58.Visible = false;
                label59.Visible = false;
                label60.Visible = false;
                label61.Visible = false;
                label62.Visible = false;
                label63.Visible = false;
                label64.Visible = false;

            }

            Parse();

            GlobalVariables.RR = 1 / (GlobalVariables.rate * 0.01667);
            //GlobalVariables.RR = Math.Round(GlobalVariables.RR, 3);
            textBox7.Text = Convert.ToString(GlobalVariables.RR);

            GlobalVariables.li = 30 / GlobalVariables.rate;

            dict.Clear();

            for (double x = 0.01; x < 3.01; x = x + 0.01 )
            {
                double pwav   = pwav_calc  (x, GlobalVariables.a_pwav, GlobalVariables.d_pwav, GlobalVariables.t_pwav, GlobalVariables.li);
                double qwav   = qwav_calc  (x, GlobalVariables.a_qwav, GlobalVariables.d_qwav, GlobalVariables.t_qwav, GlobalVariables.li);
                double qrswav = qrswav_calc(x, GlobalVariables.a_qrswav, GlobalVariables.d_qrswav, GlobalVariables.li);
                double swav   = swav_calc  (x, GlobalVariables.a_swav, GlobalVariables.d_swav, GlobalVariables.t_swav, GlobalVariables.li);
                double twav   = twav_calc  (x, GlobalVariables.a_twav, GlobalVariables.d_twav, GlobalVariables.t_twav, GlobalVariables.li);
                double uwav   = uwav_calc  (x, GlobalVariables.a_uwav, GlobalVariables.d_uwav, GlobalVariables.t_uwav, GlobalVariables.li);

                double ecg = pwav + qrswav + twav + swav + qwav + uwav;

                dict.Add(new KeyValuePair<double, double>(x, ecg));
            }
            Plot(dict);
            checkGraph();
        }

        public void Parse()
        {
            double.TryParse(textBox1.Text, out GlobalVariables.rate);
            if(GlobalVariables.rate % 60 == 0)
            {
                GlobalVariables.rate += 0000000000000000000000000000001;
            }
            double.TryParse(textBox2.Text, out GlobalVariables.a_pwav);
            double.TryParse(textBox3.Text, out GlobalVariables.d_pwav);
            double.TryParse(textBox4.Text, out GlobalVariables.t_pwav);
            double.TryParse(textBox5.Text, out GlobalVariables.a_qwav);
            double.TryParse(textBox6.Text, out GlobalVariables.d_qwav);
            GlobalVariables.t_qwav = 0.17;
            double.TryParse(textBox8.Text, out GlobalVariables.a_qrswav);
            double.TryParse(textBox9.Text, out GlobalVariables.d_qrswav);
            double.TryParse(textBox10.Text, out GlobalVariables.a_swav);
            double.TryParse(textBox11.Text, out GlobalVariables.d_swav);
            double.TryParse(textBox12.Text, out GlobalVariables.t_swav);
            double.TryParse(textBox13.Text, out GlobalVariables.a_twav);
            double.TryParse(textBox14.Text, out GlobalVariables.d_twav);
            double.TryParse(textBox15.Text, out GlobalVariables.t_twav);
            double.TryParse(textBox16.Text, out GlobalVariables.a_uwav);
            double.TryParse(textBox17.Text, out GlobalVariables.d_uwav);
            double.TryParse(textBox18.Text, out GlobalVariables.t_uwav);
        }

        public void Plot(IDictionary<double,double> dict)
        {

            chart1.Visible = true;
            if (chart1.Series.IsUniqueName("Graph"))
            {
                chart1.Series.Add("Graph");
            }
            chart1.Series[0].ChartType = SeriesChartType.Line;
            chart1.DataSource = dict;
            chart1.Series[0].XValueMember = "Key";
            chart1.Series[0].YValueMembers = "Value";
            chart1.Series[0].IsVisibleInLegend = false;
            chart1.Series[0].Color = Color.Red;

            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "0.00";
            chart1.ChartAreas[0].AxisY.LabelStyle.Format = "0.00";
            chart1.ChartAreas[0].AxisX.Title = "Time, s";
            chart1.ChartAreas[0].AxisY.Title = "ECG, mV";

            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;

            chart1.Refresh();

        }

        void chart1_MouseLeave(object sender, EventArgs e)
        {
            if (chart1.Focused)
                chart1.Parent.Focus();
        }

        void chart1_MouseEnter(object sender, EventArgs e)
        {
            if (!chart1.Focused)
                chart1.Focus();
        }

        private void chart1_MouseWheel(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Delta < 0)
                {
                    chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
                    chart1.ChartAreas[0].AxisY.ScaleView.ZoomReset();
                }

                if (e.Delta > 0)
                {
                    double xMin = chart1.ChartAreas[0].AxisX.ScaleView.ViewMinimum;
                    double xMax = chart1.ChartAreas[0].AxisX.ScaleView.ViewMaximum;
                    double yMin = chart1.ChartAreas[0].AxisY.ScaleView.ViewMinimum;
                    double yMax = chart1.ChartAreas[0].AxisY.ScaleView.ViewMaximum;

                    double posXStart = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) - (xMax - xMin) / 4;
                    double posXFinish = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.Location.X) + (xMax - xMin) / 4;
                    double posYStart = chart1.ChartAreas[0].AxisY.PixelPositionToValue(e.Location.Y) - (yMax - yMin) / 4;
                    double posYFinish = chart1.ChartAreas[0].AxisY.PixelPositionToValue(e.Location.Y) + (yMax - yMin) / 4;

                    chart1.ChartAreas[0].AxisX.ScaleView.Zoom(posXStart, posXFinish);
                    chart1.ChartAreas[0].AxisY.ScaleView.Zoom(posYStart, posYFinish);



                }
            }
            catch { }
        }

        private void chart_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            
            // If the mouse isn't on the plotting area, a datapoint, or gridline then exit
            HitTestResult htr = chart1.HitTest(e.X, e.Y);
            if (htr.ChartElementType != ChartElementType.PlottingArea && htr.ChartElementType != ChartElementType.DataPoint && htr.ChartElementType != ChartElementType.Gridlines)
                return;

            ChartArea ca = chart1.ChartAreas[0]; // Assuming you only have 1 chart area on the chart

            double xCoord = ca.AxisX.PixelPositionToValue(e.X);
            double yCoord = ca.AxisY.PixelPositionToValue(e.Y);

            e.Text = "X = " + Math.Round(xCoord, 2).ToString() + "\nY = " + Math.Round(yCoord, 2).ToString();
        }
       public double pwav_calc(double x, double a_pwav, double d_pwav, double t_pwav, double li)
        {
            double l = li;
            double a = a_pwav;
            x = x + t_pwav;
            double b = (2 * l) / d_pwav;

            double p1 = 1 / l;
            double p2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm1 = (((Math.Sin((Math.PI / (2 * b)) * (b - (2 * i)))) / (b - (2 * i)) + (Math.Sin((Math.PI / (2 * b)) * 
                    (b + (2 * i)))) / (b + (2 * i))) * (2 / Math.PI)) * Math.Cos((i * Math.PI * x) / l);
                p2 = p2 + harm1;
            }

            double pwav1 = p1 + p2;
            double pwav = a * pwav1;

            return pwav;
        }

        public double qwav_calc(double x, double a_qwav, double d_qwav, double t_qwav, double li)
        {
            double l = li;
            x = x + t_qwav;
            double a = a_qwav;
            double b = (2 * l) / d_qwav;
            double q1 = (a / (2 * b)) * (2 - b);
            double q2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm5 = (((2 * b * a) / (i * i * Math.PI * Math.PI)) * (1 - Math.Cos((i * Math.PI) / b))) * Math.Cos((i * Math.PI * x) / l);
                q2 = q2 + harm5;
            }

            double qwav = -1 * (q1 + q2);

            return qwav;
        }

        private double qrswav_calc(double x, double a_qrswav, double d_qrswav, double li)
        {
            double l = li;
            double a = a_qrswav;
            double b = (2 * l) / d_qrswav;
            double qrs1 = (a / (2 * b)) * (2 - b);
            double qrs2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm = (((2 * b * a) / (i * i * Math.PI * Math.PI)) * (1 - Math.Cos((i * Math.PI) / b))) * Math.Cos((i * Math.PI * x) / l);
                qrs2 = qrs2 + harm;
            }
            double qrswav = qrs1 + qrs2;

            return qrswav;
        }

        private double swav_calc(double x, double a_swav, double d_swav, double t_swav, double li)
        {
            double l = li;
            x = x - t_swav;
            double a = a_swav;
            double b = (2 * l) / d_swav;
            double s1 = (a / (2 * b)) * (2 - b);
            double s2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm3 = (((2 * b * a) / (i * i * Math.PI * Math.PI)) * (1 - Math.Cos((i * Math.PI) / b))) * Math.Cos((i * Math.PI * x) / l);
                s2 = s2 + harm3;
            }

            double swav = -1 * (s1 + s2);

            return swav;
        }

        private double twav_calc(double x, double a_twav, double d_twav, double t_twav, double li)
        {
            double l = li;
            double a = a_twav;
            x = x - t_twav - 0.045;
            double b = (2 * l) / d_twav;
            double t1 = 1 / l;
            double t2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm2 = (((Math.Sin((Math.PI / (2 * b)) * (b - (2 * i)))) / (b - (2 * i)) + (Math.Sin((Math.PI / (2 * b)) * (b + (2 * i)))) / (b + (2 * i))) * (2 / Math.PI)) * Math.Cos((i * Math.PI * x) / l);
                t2 = t2 + harm2;
            }
            double twav1 = t1 + t2;
            double twav = a * twav1;

            return twav;
        }
        
        private double uwav_calc(double x, double a_uwav, double d_uwav, double t_uwav, double  li)
        {
            double l = li;
            double a = a_uwav;
            x = x - t_uwav;
            double b = (2 * l) / d_uwav;
            double u1 = 1 / l;
            double u2 = 0;

            for (int i = 1; i < 101; i++)
            {
                double harm4 = (((Math.Sin((Math.PI / (2 * b)) * (b - (2 * i)))) / (b - (2 * i)) + (Math.Sin((Math.PI / (2 * b)) * (b + (2 * i)))) / (b + (2 * i))) * (2 / Math.PI)) * Math.Cos((i * Math.PI * x) / l);
                u2 = u2 + harm4;
            }

            double uwav1 = u1 + u2;
            double uwav = a * uwav1; 

            return uwav;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            defaultVariables();
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var frm = Form.ActiveForm;
            using (var bmp = new Bitmap(frm.Width, frm.Height))
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                if (String.IsNullOrEmpty(GlobalVariables.name))
                {
                    bmp.Save("Manual-" + unixTimestamp + ".jpg", ImageFormat.Jpeg);
                }
                else
                bmp.Save(GlobalVariables.name +"-"+ unixTimestamp + ".jpg", ImageFormat.Jpeg);
            }
        }


        private void defaultVariables()
        {
            textBox1.Text = "70";
            textBox2.Text = "0.23";
            textBox3.Text = "0.1";
            textBox4.Text = "0.02";
            textBox5.Text = "0.027";
            textBox6.Text = "0.04";
            //textBox7.Text = "0.17";
            textBox8.Text = "1.5";
            textBox9.Text = "0.1";
            textBox10.Text = "0.24";
            textBox11.Text = "0.07";
            textBox12.Text = "0.09";
            textBox13.Text = "0.35";
            textBox14.Text = "0.14";
            textBox15.Text = "0.2";
            textBox16.Text = "0.035";
            textBox17.Text = "0.05";
            textBox18.Text = "0.43";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dict.Count != 0)
            {
                Directory.CreateDirectory("Saved Data");
                using (StreamWriter file = new StreamWriter("Saved Data\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + GlobalVariables.name + ".txt"))
                   foreach (var entry in dict)
                   {
                       file.WriteLine("{0}", entry.Value);
                   }


            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button1.PerformClick();


            chart1.Visible = false;

            chart1.Visible = false;

            label37.Visible = true;
            label38.Visible = true;
            label39.Visible = true;
            label40.Visible = true;
            label41.Visible = true;
            label42.Visible = true;
            label43.Visible = true;
            label44.Visible = true;
            label45.Visible = true;
            label46.Visible = true;
            label48.Visible = true;
            label49.Visible = true;
            label53.Visible = true;
            label50.Visible = true;
            label51.Visible = true;
            label52.Visible = true;
            label54.Visible = true;
            label55.Visible = true;
            label56.Visible = true;
            label57.Visible = true;
            label58.Visible = true;
            label59.Visible = true;
            label60.Visible = true;
            label61.Visible = true;
            label62.Visible = true;
            label63.Visible = true;
            label64.Visible = true;


            IndexFCC();
            QTC();
            PQC();
            PQd();
            PQS();
            SI();
            FOM();
            FHS();
            fp();
            checkAssessment();
            SIcheck();

        }

        private void IndexFCC()
        {

            GlobalVariables.ptint = GlobalVariables.d_pwav + GlobalVariables.t_pwav + GlobalVariables.d_qrswav + GlobalVariables.t_twav;    

            GlobalVariables.indexfcc = 100 * (GlobalVariables.d_pwav + GlobalVariables.t_pwav) / GlobalVariables.ptint;
            GlobalVariables.indexfcc = Math.Round(GlobalVariables.indexfcc, 3);
            label46.Text = "" + GlobalVariables.indexfcc + " %";
        }

        private void QTC()
        {
            //GlobalVariables.qtc = 0.37 * Math.Sqrt(GlobalVariables.RR);
            GlobalVariables.qtc = (GlobalVariables.d_qrswav + GlobalVariables.t_twav) / Math.Pow(GlobalVariables.RR, (1.0 / 3.0));

            GlobalVariables.qtc = Math.Round(GlobalVariables.qtc, 3);
            label45.Text = "" + GlobalVariables.qtc + " s";
        }

        private void PQC()
        {
            GlobalVariables.pqc = 0.3 * GlobalVariables.qtc / 0.7;
            GlobalVariables.pqc = Math.Round(GlobalVariables.pqc, 3);
            label44.Text = "" + GlobalVariables.pqc + " s";

        }

        private void PQd()
        {
            GlobalVariables.pqd = 100 * (GlobalVariables.d_pwav + GlobalVariables.t_pwav - GlobalVariables.pqc) / (GlobalVariables.d_pwav + GlobalVariables.t_pwav);
            GlobalVariables.pqd = Math.Round(GlobalVariables.pqd, 3);
            label43.Text = "" + GlobalVariables.pqd + " %";
        }

        private void PQS()
        {
            GlobalVariables.pqs = 100 * GlobalVariables.t_pwav / (GlobalVariables.d_pwav + GlobalVariables.t_pwav);
            GlobalVariables.pqs = Math.Round(GlobalVariables.pqs, 3);
            label42.Text = "" + GlobalVariables.pqs + " %";
        }
        
        private void SI()
        {
            GlobalVariables.SI = 100* (GlobalVariables.d_qrswav + GlobalVariables.t_twav) / GlobalVariables.RR;
            GlobalVariables.SI = Math.Round(GlobalVariables.SI, 3);
            label48.Text = "" + GlobalVariables.SI + " %";
        }

        private void FHS()
        {
            double fhs = 90 / (GlobalVariables.ptint);
            label50.Text = "" + Math.Round(fhs, 3) + " bpm";

        }

        private void FOM()
        {
            GlobalVariables.fom = 100 * (1 - (GlobalVariables.ptint / GlobalVariables.RR));
            label52.Text = "" + Math.Round(GlobalVariables.fom, 3) + " %";
        }
        private void fp()
        {
            GlobalVariables.fp = 100 * (GlobalVariables.t_swav + 0.5 * GlobalVariables.d_twav) / (GlobalVariables.d_qrswav + GlobalVariables.t_twav);
            GlobalVariables.fp = Math.Round(GlobalVariables.fp, 3);
            if((GlobalVariables.fp == 0))
            {
                label54.Visible = false;
                label55.Visible = false;
            }
            label54.Text = "" + GlobalVariables.fp + " %";
        }

        private bool emptyLabel()
        {
            if (string.IsNullOrWhiteSpace(label47.Text))
            {
                return true;
            }
            return false;
        }

        private void SIcheck()
        {
            int[,] matrix = new int[3, 9] { { 40, 50, 60, 70, 80, 90, 100, 110, 120 }, { 30, 33, 37, 40, 43, 45, 47, 50, 52 }, { 32, 36, 40, 43, 47, 48, 50, 54, 56 } };
            
            int SI_check = 0;

            for (int i = 0; i < 8; i++)
            {
                if (GlobalVariables.rate >= matrix[0,i] && GlobalVariables.rate < matrix[0, i + 1])
                {
                    char last = GlobalVariables.name[GlobalVariables.name.Length - 1];
                    if(last == 'M' || last == 'm')
                    {
                        SI_check = matrix[1, i];
                    }
                    else if (last == 'F' || last == 'f')
                    {
                        SI_check = matrix[2, i];
                    }
                    
                    if((Math.Abs(GlobalVariables.SI - SI_check) < 5 && Math.Abs(GlobalVariables.SI - SI_check) > 7))
                    {
                        label47.Text = label47.Text + "\nSI Моментен риск за нарушение на електрическата систола на камерите";
                    }
                }
            }
        }

        private void checkAssessment()
        {
            label47.Text = "";
            label47.TextAlign = ContentAlignment.MiddleLeft;
            if (GlobalVariables.indexfcc < 28 || GlobalVariables.indexfcc > 32)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "FCC Моментен риск за ускорение/забавяне на AV-проводимостта";
                    //label47.Text = label47.Text + "\n" + "FCC Моментен риск за ускорение/забавяне на AV-проводимостта";
                else label47.Text = "Fcc FCC Моментен риск за ускорение/забавяне на AV-проводимостта";

            }
            if (Math.Abs(GlobalVariables.pqd) >= 10)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "PQD Моментен риск за промяна на AV-проводимостта";
                else label47.Text = "PQD Моментен риск за промяна на AV-проводимостта";
                //else label47.Text = "PQD Моментен риск за промяна на AV-проводимостта";

            }
            if (GlobalVariables.pqs <= 25)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "PQS Моментен риск за нарушен ритъм";
                //else label47.Text = "PQS Моментен риск за нарушен ритъм";
                else label47.Text = "PQS Моментен риск за нарушен ритъм";
            }
            if (((GlobalVariables.d_pwav + GlobalVariables.t_pwav) < 0.120 && (GlobalVariables.pqs > 17.5 && GlobalVariables.pqs < 18)))
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "PQS* Моментен риск за нарушен PQ интервал";
                    //else label47.Text = "PQS* Моментен риск за нарушен PQ интервал";
                else label47.Text = "PQS* Моментен риск за нарушен PQ интервал";

            }
            if ((GlobalVariables.d_pwav + GlobalVariables.t_pwav) > 0.120 && (GlobalVariables.d_pwav + GlobalVariables.t_pwav) < 0.140 && GlobalVariables.pqs > 18 && GlobalVariables.pqs <= 18.5)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "PQS* Моментен риск за нарушен PQ интервал";
                    //label47.Text = label47.Text + "\n" + "PQS* Моментен риск за нарушен PQ интервал";
                else label47.Text = "PQS* Моментен риск за нарушен PQ интервал";

            }
            if ((GlobalVariables.d_pwav + GlobalVariables.t_pwav) > 0.140 && GlobalVariables.pqs > 31 && GlobalVariables.pqs < 32 )
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "PQS* Моментен риск за нарушен PQ интервал";
                else label47.Text = "PQS* Моментен риск за нарушен PQ интервал";

            }
            if(GlobalVariables.fom <= 35 || GlobalVariables.fom >= 50)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "FOM Моментен риск за структурни и функционални патологии";
                    //label47.Text = label47.Text + "\n" + "FOM Моментен риск за структурни и функционални патологии";
                else label47.Text = "FOM Моментен риск за структурни и функционални патологии";
            }
            //double test = GlobalVariables.fp;
            if(GlobalVariables.fp < 45)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "FP Моментен риск";
                else label47.Text = "FP Моментен риск";

            }

            if (string.IsNullOrWhiteSpace(label47.Text))
            {
                label47.TextAlign = ContentAlignment.MiddleCenter;
                label47.Text = "Нормален ЕКГ ритъм";
            }
        }

        private void checkGraph()
        {
            label47.Text = "";
            label47.TextAlign = ContentAlignment.MiddleLeft;
            if (GlobalVariables.a_pwav > 0.25 || GlobalVariables.d_pwav > 0.12)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "Претоварена P-вълна";
                else label47.Text = "Претоварена P-вълна";
            }
            //PQ proverka
            if ((GlobalVariables.d_pwav + GlobalVariables.t_pwav) > 0.21)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "AV-блок";
                else label47.Text = "AV-блок";
            } 
            if ((GlobalVariables.d_pwav + GlobalVariables.t_pwav) < 0.12 && GlobalVariables.d_qrswav >= 0.12)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "WPW-синдром";
                else label47.Text = "WPW-синдром";
            }
            if ((GlobalVariables.d_pwav + GlobalVariables.t_pwav) < 0.12 && GlobalVariables.d_qrswav <= 0.10)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "LGL-синдром";
                else label47.Text = "LGL-синдром";
            }
            if(GlobalVariables.a_qwav >= GlobalVariables.a_qrswav / 4 || GlobalVariables.d_qwav > 0.04)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "Абнормална Q-вълна";
                else label47.Text = "Абнормална Q-вълна";
            }
            if(GlobalVariables.a_twav < GlobalVariables.a_qrswav / 8 || GlobalVariables.a_twav > 2 * GlobalVariables.a_qrswav / 3)
            {
                if (!emptyLabel())
                    label47.Text = label47.Text + "\n" + "Абнормална T-вълна";
                else label47.Text = "Абнормална T-вълна";
            }

            if (string.IsNullOrWhiteSpace(label47.Text))
            {
                label47.TextAlign = ContentAlignment.MiddleCenter;
                label47.Text = "Нормален ЕКГ ритъм";
            }
        }
    }
}