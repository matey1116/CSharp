﻿namespace Graphic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea16 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend16 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea17 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend17 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea18 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend18 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea19 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend19 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea20 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend20 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea21 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend21 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea22 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend22 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea23 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend23 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea24 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend24 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea25 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend25 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea26 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend26 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea27 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend27 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea28 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend28 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series28 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea29 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend29 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series29 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea30 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend30 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series30 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart7 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart8 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart9 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart10 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chart11 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart12 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart13 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart14 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart15 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart15)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea16.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea16);
            legend16.Name = "Legend1";
            this.chart1.Legends.Add(legend16);
            this.chart1.Location = new System.Drawing.Point(178, 12);
            this.chart1.Name = "chart1";
            series16.ChartArea = "ChartArea1";
            series16.Legend = "Legend1";
            series16.Name = "Series1";
            this.chart1.Series.Add(series16);
            this.chart1.Size = new System.Drawing.Size(1057, 129);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea17.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea17);
            legend17.Name = "Legend1";
            this.chart2.Legends.Add(legend17);
            this.chart2.Location = new System.Drawing.Point(178, 147);
            this.chart2.Name = "chart2";
            series17.ChartArea = "ChartArea1";
            series17.Legend = "Legend1";
            series17.Name = "Series1";
            this.chart2.Series.Add(series17);
            this.chart2.Size = new System.Drawing.Size(1057, 129);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // chart3
            // 
            chartArea18.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea18);
            legend18.Name = "Legend1";
            this.chart3.Legends.Add(legend18);
            this.chart3.Location = new System.Drawing.Point(178, 282);
            this.chart3.Name = "chart3";
            series18.ChartArea = "ChartArea1";
            series18.Legend = "Legend1";
            series18.Name = "Series1";
            this.chart3.Series.Add(series18);
            this.chart3.Size = new System.Drawing.Size(1057, 129);
            this.chart3.TabIndex = 2;
            this.chart3.Text = "chart3";
            // 
            // chart4
            // 
            chartArea19.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea19);
            legend19.Name = "Legend1";
            this.chart4.Legends.Add(legend19);
            this.chart4.Location = new System.Drawing.Point(178, 417);
            this.chart4.Name = "chart4";
            series19.ChartArea = "ChartArea1";
            series19.Legend = "Legend1";
            series19.Name = "Series1";
            this.chart4.Series.Add(series19);
            this.chart4.Size = new System.Drawing.Size(1057, 129);
            this.chart4.TabIndex = 3;
            this.chart4.Text = "chart4";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(24, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 32);
            this.button1.TabIndex = 4;
            this.button1.Text = "Select File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1078, 552);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(157, 40);
            this.button3.TabIndex = 6;
            this.button3.Text = "Next Charts";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(915, 552);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 40);
            this.button4.TabIndex = 7;
            this.button4.Text = "Previous Charts";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // chart5
            // 
            chartArea20.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea20);
            legend20.Name = "Legend1";
            this.chart5.Legends.Add(legend20);
            this.chart5.Location = new System.Drawing.Point(178, 417);
            this.chart5.Name = "chart5";
            series20.ChartArea = "ChartArea1";
            series20.Legend = "Legend1";
            series20.Name = "Series1";
            this.chart5.Series.Add(series20);
            this.chart5.Size = new System.Drawing.Size(510, 129);
            this.chart5.TabIndex = 8;
            this.chart5.Text = "chart5";
            // 
            // chart6
            // 
            chartArea21.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea21);
            legend21.Name = "Legend1";
            this.chart6.Legends.Add(legend21);
            this.chart6.Location = new System.Drawing.Point(725, 417);
            this.chart6.Name = "chart6";
            series21.ChartArea = "ChartArea1";
            series21.Legend = "Legend1";
            series21.Name = "Series1";
            this.chart6.Series.Add(series21);
            this.chart6.Size = new System.Drawing.Size(510, 129);
            this.chart6.TabIndex = 9;
            this.chart6.Text = "chart6";
            // 
            // chart7
            // 
            chartArea22.Name = "ChartArea1";
            this.chart7.ChartAreas.Add(chartArea22);
            legend22.Name = "Legend1";
            this.chart7.Legends.Add(legend22);
            this.chart7.Location = new System.Drawing.Point(178, 12);
            this.chart7.Name = "chart7";
            series22.ChartArea = "ChartArea1";
            series22.Legend = "Legend1";
            series22.Name = "Series1";
            this.chart7.Series.Add(series22);
            this.chart7.Size = new System.Drawing.Size(1057, 187);
            this.chart7.TabIndex = 10;
            this.chart7.Text = "chart7";
            // 
            // chart8
            // 
            chartArea23.Name = "ChartArea1";
            this.chart8.ChartAreas.Add(chartArea23);
            legend23.Name = "Legend1";
            this.chart8.Legends.Add(legend23);
            this.chart8.Location = new System.Drawing.Point(178, 211);
            this.chart8.Name = "chart8";
            series23.ChartArea = "ChartArea1";
            series23.Legend = "Legend1";
            series23.Name = "Series1";
            this.chart8.Series.Add(series23);
            this.chart8.Size = new System.Drawing.Size(1057, 187);
            this.chart8.TabIndex = 11;
            this.chart8.Text = "chart8";
            // 
            // chart9
            // 
            chartArea24.Name = "ChartArea1";
            this.chart9.ChartAreas.Add(chartArea24);
            legend24.Name = "Legend1";
            this.chart9.Legends.Add(legend24);
            this.chart9.Location = new System.Drawing.Point(178, 12);
            this.chart9.Name = "chart9";
            series24.ChartArea = "ChartArea1";
            series24.Legend = "Legend1";
            series24.Name = "Series1";
            this.chart9.Series.Add(series24);
            this.chart9.Size = new System.Drawing.Size(1057, 247);
            this.chart9.TabIndex = 12;
            this.chart9.Text = "chart9";
            // 
            // chart10
            // 
            chartArea25.Name = "ChartArea1";
            this.chart10.ChartAreas.Add(chartArea25);
            legend25.Name = "Legend1";
            this.chart10.Legends.Add(legend25);
            this.chart10.Location = new System.Drawing.Point(178, 299);
            this.chart10.Name = "chart10";
            series25.ChartArea = "ChartArea1";
            series25.Legend = "Legend1";
            series25.Name = "Series1";
            this.chart10.Series.Add(series25);
            this.chart10.Size = new System.Drawing.Size(1057, 247);
            this.chart10.TabIndex = 13;
            this.chart10.Text = "chart10";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(24, 114);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 32);
            this.button5.TabIndex = 14;
            this.button5.Text = "Exit";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(24, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 32);
            this.button2.TabIndex = 15;
            this.button2.Text = "ScreenShot";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chart11
            // 
            chartArea26.Name = "ChartArea1";
            this.chart11.ChartAreas.Add(chartArea26);
            legend26.Name = "Legend1";
            this.chart11.Legends.Add(legend26);
            this.chart11.Location = new System.Drawing.Point(178, 12);
            this.chart11.Name = "chart11";
            series26.ChartArea = "ChartArea1";
            series26.Legend = "Legend1";
            series26.Name = "Series1";
            this.chart11.Series.Add(series26);
            this.chart11.Size = new System.Drawing.Size(1057, 534);
            this.chart11.TabIndex = 17;
            this.chart11.Text = "chart11";
            // 
            // chart12
            // 
            chartArea27.Name = "ChartArea1";
            this.chart12.ChartAreas.Add(chartArea27);
            legend27.Name = "Legend1";
            this.chart12.Legends.Add(legend27);
            this.chart12.Location = new System.Drawing.Point(178, 13);
            this.chart12.Name = "chart12";
            series27.ChartArea = "ChartArea1";
            series27.Legend = "Legend1";
            series27.Name = "Series1";
            this.chart12.Series.Add(series27);
            this.chart12.Size = new System.Drawing.Size(1057, 162);
            this.chart12.TabIndex = 19;
            this.chart12.Text = "chart12";
            // 
            // chart13
            // 
            chartArea28.Name = "ChartArea1";
            this.chart13.ChartAreas.Add(chartArea28);
            legend28.Name = "Legend1";
            this.chart13.Legends.Add(legend28);
            this.chart13.Location = new System.Drawing.Point(178, 199);
            this.chart13.Name = "chart13";
            series28.ChartArea = "ChartArea1";
            series28.Legend = "Legend1";
            series28.Name = "Series1";
            this.chart13.Series.Add(series28);
            this.chart13.Size = new System.Drawing.Size(1057, 162);
            this.chart13.TabIndex = 20;
            this.chart13.Text = "chart13";
            // 
            // chart14
            // 
            chartArea29.Name = "ChartArea1";
            this.chart14.ChartAreas.Add(chartArea29);
            legend29.Name = "Legend1";
            this.chart14.Legends.Add(legend29);
            this.chart14.Location = new System.Drawing.Point(178, 384);
            this.chart14.Name = "chart14";
            series29.ChartArea = "ChartArea1";
            series29.Legend = "Legend1";
            series29.Name = "Series1";
            this.chart14.Series.Add(series29);
            this.chart14.Size = new System.Drawing.Size(1057, 162);
            this.chart14.TabIndex = 21;
            this.chart14.Text = "chart14";
            // 
            // chart15
            // 
            chartArea30.Name = "ChartArea1";
            this.chart15.ChartAreas.Add(chartArea30);
            legend30.Name = "Legend1";
            this.chart15.Legends.Add(legend30);
            this.chart15.Location = new System.Drawing.Point(178, 12);
            this.chart15.Name = "chart15";
            series30.ChartArea = "ChartArea1";
            series30.Legend = "Legend1";
            series30.Name = "Series1";
            this.chart15.Series.Add(series30);
            this.chart15.Size = new System.Drawing.Size(1057, 533);
            this.chart15.TabIndex = 22;
            this.chart15.Text = "chart15";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1247, 604);
            this.Controls.Add(this.chart15);
            this.Controls.Add(this.chart14);
            this.Controls.Add(this.chart13);
            this.Controls.Add(this.chart12);
            this.Controls.Add(this.chart11);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.chart10);
            this.Controls.Add(this.chart9);
            this.Controls.Add(this.chart8);
            this.Controls.Add(this.chart7);
            this.Controls.Add(this.chart6);
            this.Controls.Add(this.chart5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart4);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "AMEG graphics";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart15)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart9;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart11;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart12;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart14;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart15;
    }
}

